package cookie

import (
	"net/http"
	"time"
)

const COOKIE_TIME = 60 * 60 * 24 * 14

type Custom_Cookie struct {
	http.Cookie
}

// Cookie create
func CookieCreate(name string, value string, host string, optional ...int) *http.Cookie {
	timeExpires := time.Now().Add(time.Duration(COOKIE_TIME) * time.Second)
	maxAge := COOKIE_TIME
	// maxAge := 0

	// If cookie need down
	if len(optional) > 0 {
		if optional[0] == -1 {
			maxAge = -1
			timeExpires = time.Now()
		}
	}

	return &http.Cookie{
		Name:  name,
		Value: value,
 		Domain:  host,
		MaxAge:  maxAge,
		Path:    `/`,
		Expires: timeExpires,
	}
}

// Read cookie by name
func CookieGet(r_http *http.Response, name string) *http.Cookie {
	if r_http != nil {
		for _, val := range r_http.Cookies() {
			if val.Name == name {
				return val
			}
		}
	}
	return nil
}
